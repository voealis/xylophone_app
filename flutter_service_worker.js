'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  ".git/COMMIT_EDITMSG": "dd3f743361e7ba89e69d9bac2ed0fdf1",
".git/config": "2857c560e6531e4d795f1ef9a93fe5c8",
".git/description": "a0a7c3fff21f2aea3cfa1d0316dd816c",
".git/HEAD": "4cf2d64e44205fe628ddd534e1151b58",
".git/hooks/applypatch-msg.sample": "ce562e08d8098926a3862fc6e7905199",
".git/hooks/commit-msg.sample": "579a3c1e12a1e74a98169175fb913012",
".git/hooks/fsmonitor-watchman.sample": "a0b2633a2c8e97501610bd3f73da66fc",
".git/hooks/post-update.sample": "2b7ea5cee3c49ff53d41e00785eb974c",
".git/hooks/pre-applypatch.sample": "054f9ffb8bfe04a599751cc757226dda",
".git/hooks/pre-commit.sample": "305eadbbcd6f6d2567e033ad12aabbc4",
".git/hooks/pre-merge-commit.sample": "39cb268e2a85d436b9eb6f47614c3cbc",
".git/hooks/pre-push.sample": "2c642152299a94e05ea26eae11993b13",
".git/hooks/pre-rebase.sample": "56e45f2bcbc8226d2b4200f7c46371bf",
".git/hooks/pre-receive.sample": "2ad18ec82c20af7b5926ed9cea6aeedd",
".git/hooks/prepare-commit-msg.sample": "2b5c047bdb474555e1787db32b2d2fc5",
".git/hooks/push-to-checkout.sample": "c7ab00c7784efeadad3ae9b228d4b4db",
".git/hooks/update.sample": "647ae13c682f7827c22f5fc08a03674e",
".git/index": "9f1e7c0f51ffe56fcb2ea2290feabfda",
".git/info/exclude": "036208b4a1ab4a235d75c181e685e5a3",
".git/logs/HEAD": "53d31c681b1fbd16590572409596c043",
".git/logs/refs/heads/master": "53d31c681b1fbd16590572409596c043",
".git/objects/00/fa4417cfbef8673c47c86eb24033fcd97056a8": "45debb2445f3f0841e9128c562f63708",
".git/objects/06/952be745f9fa6fa75196e830d9578eb2ee631d": "cb6848767aab99a89a4ec04efbc00625",
".git/objects/08/7e2ec47b8e4f2d6f8738f639224d3bbd8999ac": "82b4fd37f8e4003ca99a4ebd0b3d0aae",
".git/objects/0a/3f5fa40fb3d1e0710331a48de5d256da3f275d": "15924b1217c401aba33f13069f7d81d5",
".git/objects/0b/edcf2fd46788ae3a01a423467513ff59b5c120": "23a6b285e31cde5ab29a2fc1ebfdb935",
".git/objects/0f/816fb5068fb5d0dc1623718a94d7a34c5edfe4": "48392ce692d6328aef69a753fa305233",
".git/objects/11/39b653c23aa05dbdb02fadbabc7672b8e8de68": "7dbce579db112d1e6e400f00658e83dd",
".git/objects/11/cc2144efc0dfadbee0e6e9a047f7f5d2d2d14b": "51f32bc3c21e1fca16a9a6031a7f90ed",
".git/objects/13/b35eba55c6dabc3aac36f33d859266c18fa0d0": "ccb5b80ef3b4fb1030a8a4f520f3f5e8",
".git/objects/14/181eaed80669af2088550fe575af5741ca5b4b": "33c62d2a1ea0c6f2a112bb45bff07204",
".git/objects/15/9b15011009e1e691b0761e5627dc3f3aa452ad": "822c0ced8e90203ed87ff7e0082d7945",
".git/objects/16/7494e6435f38be767aa6ef0582474d82e02fda": "9c073f2c3b9c31009edb64520af31d52",
".git/objects/17/411a8ab8eb7a538e7dfa59b33869832f6f0269": "0d9f72720791d60ee583bd3e78ebe949",
".git/objects/17/ba431125b4b5e7df0c768da15f4aded702e6b9": "69fd2fef6bd1c5ceb6767eee7600792a",
".git/objects/17/e9516a74074fad22bac3e52e39131886697622": "2cccf0b2cd7b787a1f969f9552b76de3",
".git/objects/18/97a15dc27b07db087508ba8590213cf0e32ce7": "d6fb3046bb4de1e500e0988f3a6512c1",
".git/objects/18/d981003d68d0546c4804ac2ff47dd97c6e7921": "318597cf2655c5770342bae4ab7b90a3",
".git/objects/1a/b6015dd9799d6c64fdc50c3b75dba6195e6aec": "484018ed0c38fbfa5927df7c9227d562",
".git/objects/1a/b847b818dec389fb43fb9da80637c02e27d3d3": "8af286f2a069534106d53e8c037b0a4a",
".git/objects/1b/2cb5d6d09fe5019d2f930632eaa15b4e51aedc": "9adea2f748ae31c67040537f1b6e580e",
".git/objects/1d/526a16ed0f1cd0c2409d848bf489b93fefa3b2": "12bcdfd9683b55567627456d0773a4e5",
".git/objects/21/0247ecc410ae24d9c884aa6f0cff45c95bda2c": "e7121a280d4a53ed6f28715eba8c7807",
".git/objects/21/19932de20217fe08ca9135659fddd4ac333f0d": "614d51d471aa124a5db88fb9339f962a",
".git/objects/21/9664399f2a08943a0cabf4020e9d174dbdee93": "67cd9840329de30371a52b3acd5aef6c",
".git/objects/24/08e30d1751ecd910eace02fa947f67c0bbd1dd": "a0e8763e79dd07bb8cba58a68b899f03",
".git/objects/24/b918b2d4adcf2fcb9688aa61d4fdeb3fad058f": "bcc66a022fbbc88467d306ab064fc5af",
".git/objects/27/22837ec9181c3c1b2f1c1298870185c03ba354": "55274092f8c707defc529cc6940e63ee",
".git/objects/28/935bb35d5eb0818c0d2f67e19f9697fbe55aa8": "0fc6584a66880774260778818dec6acd",
".git/objects/28/c6bf03016f6c994b70f38d1b7346e5831b531f": "859e347621927457ff896dba865a18fd",
".git/objects/2b/2c3a562b375d8b8666585df340e8f3868f38a8": "92a5ead6e841d0afb413c30b02850499",
".git/objects/2c/cbfd967d9697cd4b83225558af2911e9571c9b": "79f574379587d27e91d5ebf05ea6e777",
".git/objects/2d/6faea19434073f5023d687733791b55f727183": "1367360686ac50fef1fcd7775685ce2d",
".git/objects/2e/1de87a7eb61e17463f7406106f6c413533cecf": "f4970286a19c04f87217910d26430ddf",
".git/objects/2f/1632cfddf3d9dade342351e627a0a75609fb46": "eebc09df6f2f2370e95a1eb0bc1fb2c5",
".git/objects/30/4732f8842013497e14bd02f67a55f2614fb8f7": "76eb293b5c9304707852ee681a90e8fc",
".git/objects/31/5d994bf53962fa91208a759c84990818056958": "bedee235175940f0f489e9098382912f",
".git/objects/32/6c0e72c9d820600887813b3b98d0dd69c5d4e8": "3030e2c76fcd41356672eee54ca3bd94",
".git/objects/32/fd4921262a09b42f8d61ad1a9f079f90d98ec8": "506c50a66c4fdeda2ea761ed30ea0df8",
".git/objects/34/7139c48727d4f32a2b9bac862fb4793356cc63": "f12c4e44afc2ef65f4df0d401fdf25b5",
".git/objects/36/9b9692a70d8cbee869c70c128573fd8ca8c5df": "11a883eb3322c71911e5e91be1fb6948",
".git/objects/36/b0fd9464f45b33f482e64bea579787e142affa": "9d308f3efbc512e2b01277fb7565acef",
".git/objects/36/e21bbf9cf407bfa7968f28d35675da72c6c6c0": "0afd293643544b95c72e3b0baadf56aa",
".git/objects/38/79d54755798567f0f318d63340508d5668eb96": "cfe50cd405acbf1c250baa58cfe6b9ed",
".git/objects/39/1e332ca96bc6e5aabfcad265fcd36814f62ccf": "0a6efaf859c3f2fe04c89126cbb6d810",
".git/objects/3f/7682a6e830e34f27e98a37d386fa63b2985df4": "1acac7a08041d4c873c649de6751474c",
".git/objects/40/afcd9eb086f844b8743783c56e706046b15bde": "b80f08d0697764b3306bda30b324f804",
".git/objects/42/bcbf4780b187e80025b7917058bcb882012aa1": "d13c60a94845ee93e5124ec90980bb48",
".git/objects/46/4ab5882a2234c39b1a4dbad5feba0954478155": "2e52a767dc04391de7b4d0beb32e7fc4",
".git/objects/46/a4e480666c7a21e6335c41e71d61056ae48c48": "9a32a648482e2255f8e1af46a411a624",
".git/objects/47/89daa6a443eefa7f3f729164a18351e06e9800": "3c2dcd6a17cdfc0957293d41ef712d24",
".git/objects/48/7aeea8702c0314f991f9be34ac70693532c004": "60554e36f70e51308d6545acc84db43d",
".git/objects/4a/7c0e4288e2c695dffc2df921daf6158e942b20": "38c518c054ea97d68ddaeb7fddffa87c",
".git/objects/4a/8ceafcdceb2c29493f8f9eb8b462cdb6a414b5": "1f52b02db34419674356f701388f287d",
".git/objects/4c/de12118dda48d71e01fcb589a74d069c5d7cb5": "28e8a446706511f1a67da714824896ea",
".git/objects/4d/29816a247afc8ce19d4669036a14e4dae9157d": "1002b456afd7753abbf03c18c6f0f8dd",
".git/objects/4f/6e9adeed90ed29bdfd10ec04d1c56a70078dd6": "fb77d0496186f8c544c81ff810ab9ca0",
".git/objects/52/a6eff3130374fa3f4f824ea6f36863dd3e64b3": "d9d4591c48400be4ab4f55824fc0ff60",
".git/objects/53/153f08aebc5d40d3e0ca1c360736775366694f": "987f8f64c8bc6d7da3df8582dae6ba0d",
".git/objects/53/f0ce06bd128540caf74fccfb448b654344a2d6": "7e245e9132dc2b81dce0078320eb6e3e",
".git/objects/54/1a1aca9fc21d977e62f38ff9bca2b2420b363f": "c19a196b14330f95b39e6c957154c9ce",
".git/objects/55/25959088a9d1e71788f48aaede6133d8c8705b": "1fe3607f6f37d758295dce7a73ff8612",
".git/objects/58/ed75652088d454b762c0b05209308b16ef7740": "8f47ad2a9ed41720d60310643bad2656",
".git/objects/59/2ceee85b89bd111b779db6116b130509ab6d4b": "348329ef452b809389b6496387db6913",
".git/objects/59/a72e90be12e0aef464a1c3173b632db6237fd8": "bbdaf9e38c66977c852dc8e6c7720d46",
".git/objects/5a/01ac95fcfb433e889bb54f6f7e9d10a312cc4e": "9d725e3e6d88763902fb91ade339a8b1",
".git/objects/5a/2f14fb18f6e8b8c4308ff0f0dc187d9d27a5aa": "cd730fc3b9291a1a04788c027bfd6f37",
".git/objects/5a/60bcfc61c6dd56bea794a62996dec8ed93840c": "3eed424a6892a52d3ab5ceb2a477a303",
".git/objects/5d/f41d96c023a33e4d2ada17678ee35ea3a0c77a": "4f8553fd499a7015e3e4cb5879bb9a78",
".git/objects/5f/780e59bb2e2d15a2e6691d2e45d1b56de94ea2": "2be7ce3db2d1524e5760360faf39a37a",
".git/objects/60/518d295956d9580c0acb7ee5a903b4bdc21250": "22c1d6d01faf7fc1d8fdea62943b78d8",
".git/objects/61/b6c4de17c96863d24279f06b85e01b6ebbdb34": "5486bd6a506a32f8c081890c2befb8eb",
".git/objects/66/6b2dc8cf20f6f8094d9a639a0169c3c395f5ee": "3208a8cd06e0410260dfc3292e794135",
".git/objects/66/a65d1e4a79f230031ec0e0959a721039e7282f": "2a87e723b46c44522c16c0968efa0152",
".git/objects/67/6c12c1a2b1fa100be943623b558f861f5e65dc": "f8c99f4416e6871d9542aff4836daf1b",
".git/objects/69/33725ebcdea466e35c5714c3a8fb26307a4ead": "8ec4e93b8af24fb0f4c508cd7a46b153",
".git/objects/69/b3e43e1a555b28375bc2ecc8d9eac0abf8b402": "494bbd6e669e6b86ff22ae69637d84ee",
".git/objects/6a/84f41e14e27f4b11f16f9ee39279ac98f8d5ac": "a913aaf750cc736879f14f53488f5c42",
".git/objects/6a/8727f27c2651605525092f6a910b5d12bea5ac": "eaeedcbd08024f8d4b82afd8a69c524c",
".git/objects/6a/abe4645643fb83c8a9caae7617c1f7cdb2835e": "18055395d9d0acb10788d82066b89c8f",
".git/objects/6c/8e736c9131cbed40246aad6c478f4be38fbd3f": "f2b1e79be5b3a72ec8b64640a7d3e089",
".git/objects/6d/560ec129c739c2963722e4ab19e9bb3717c5f4": "620315f3ff1a741899185e645578a317",
".git/objects/6d/a0652f05f28fc6950cec20ebdfbae89b665479": "fccd5f0f0fbfe5ead1a5622148fb8d74",
".git/objects/6e/b1fdc72bc900bd00dd6e6e2c62bb44428360b5": "ef300f8e5036e59882961fd88fa76ca9",
".git/objects/6f/568019d3c69d4966bb5a0f759980a1472afc1e": "11711ec452167a523e427fab835abd15",
".git/objects/70/693e4a8c128fc4350b157416374ca599ac8c7b": "6942d9ddf2906a8e847e7ef6889a4221",
".git/objects/72/271d5e41701cfbffad74d38640bf9cc7c1f7be": "e07153a480d4e159b70f80851f6a553d",
".git/objects/74/6adbb6b9e14b7e685c91e280a4d37a672afbd8": "eba295c22165b01ec06a735e4dbb3a44",
".git/objects/75/b2d164a5a98e212cca15ea7bf2ab5de5108680": "d453ef5dc75a6b2e158c081f7a51acac",
".git/objects/76/d73547d5356234aacd35576bb5e3b61ec0353e": "2d584ed7d5c4a8b7862fa3c170e33da1",
".git/objects/76/fcede677ad0498cf0d51052e64844ea6131eca": "266ec465560cd6c6bbfb26440339685f",
".git/objects/78/6d6aad54578a4ed6d63bff4bec11a3e6b66926": "6771f0d28ea8ce14b038ce99414ed41e",
".git/objects/7a/349cac37759d2e67b7414d18ca9e3f982bb2ef": "3207547fd7f963b827daa891515809cb",
".git/objects/7a/7f9873ad7dceb4dc17087fb06c800fa0191376": "91f6523d91623d19d122fea7b32a3db9",
".git/objects/7a/ed27f7f6177f236cd6a253826ffa6314d9818c": "9a357c2b3d09bbc517269fdc0f52c35a",
".git/objects/7b/045bcf727ace9f21c41ea5221a3f27b67416e3": "512c5731c474668192c57dc80f633bad",
".git/objects/7c/fb6f962bdb8c4b610bb1bb25b6c4361f9d20e8": "20235224869b650fc9ceb371e56bb5c9",
".git/objects/7f/78f3e6c74146f76ebb158203c567634f87900f": "ab2787ecadd061631a703202b5949d60",
".git/objects/80/e867a4e06b4dc26d0a2b327cbd54041addc50a": "a256672b7db051545b2b39eb821c1510",
".git/objects/81/0337fcab9374ea7916511a5b9b59c1fe38c5fd": "cc99e87ef5a5ad26f76eb93e555d98fa",
".git/objects/81/8c410ed0ddd3e693a8800ceb78acd0f8920112": "b7226ee8f9390dcef18667e2aa983b94",
".git/objects/82/89838a9d1b8419e109d57dba08acd2bd17a40b": "1bcd0571700890f99ba2ce59bca0a88b",
".git/objects/82/b668fe500dbbb0b6d0ccf43bba85287ff1251c": "7279336f2468cdd38231c9f16fe529d6",
".git/objects/82/b6f9d9a33e198f5747104729e1fcef999772a5": "32d0349a1626c50624f6aab0900c31d8",
".git/objects/83/23c7e43336b5cef23a4247bcf6b504a90bcc8c": "c6587c55c8b6664afd5eca9876e78b3d",
".git/objects/85/2fa1a4728ae4789e3bca55dd07caef3b41f2a5": "97bf964ac33d3f622d940921761e740c",
".git/objects/86/9f0c30749653d347a4639407a293a466dc4d89": "faf02fe4d752cfba9930ba913ace7cf3",
".git/objects/88/9b01bfed6746066cc200a7fdb13eb9ef79036f": "7bbbcba971ace4b04f41978ec90a23b5",
".git/objects/88/cfd48dff1169879ba46840804b412fe02fefd6": "e42aaae6a4cbfbc9f6326f1fa9e3380c",
".git/objects/89/c2725b70f1882be97f5214fafe22d27a0ec01e": "99b17ebd650decc04c5de3b0893b27f7",
".git/objects/8a/aa46ac1ae21512746f852a42ba87e4165dfdd1": "1d8820d345e38b30de033aa4b5a23e7b",
".git/objects/8a/f96a2c8ade099adb3fca57791d512ff901fc4a": "d25ee2521973bcf7a4fc7ba6e7142677",
".git/objects/8b/6d4680af388f28db8742ef7fb8246e2bb1fffb": "4e20d4660135ff8d41852fa5b986bfb5",
".git/objects/8b/d06bdf095c809f80d639847440b06b0199a88c": "58d6dcf8e95c016f9f2dd6468f1fcdf1",
".git/objects/8d/4492f977adc7dd7a836405d4916e5c9c014536": "17812516aa18e24296a565b4fbad60b5",
".git/objects/91/8dcd1243c72e095407bba45a2fdb595a497de7": "b4d2ffe02ea0dc15874e843991713b39",
".git/objects/91/9434a6254f0e9651f402737811be6634a03e9c": "6da5342c04a33ac2131fdae2e4a84892",
".git/objects/93/0d2071a324ee6050cccd87a14495557b63416f": "27184823c9e5055a30e98ccf70968672",
".git/objects/97/8746b5424d1212460133977131fc5ef0971abb": "3cc094294d4d3275ff02b12eff5e10c3",
".git/objects/9c/1373b36ee911329cdb1830e7f1e4cc5ac720b2": "d18f10e9755d87aa73b25ebe483e79cc",
".git/objects/9d/a19eacad3b03bb08bbddbbf4ac48dd78b3d838": "1176aaa6ff7a19c56cf9a6ea692a2c03",
".git/objects/9e/949aa96e4deab4655ecf184422e0810b6b128f": "569ae7a9f0f11787d370b19499e04002",
".git/objects/a2/ec33f19f110ebff51a23342d7bc29ec9a1aaa6": "48569d95657b1f4b8fad6b6e730d4334",
".git/objects/a4/2ea7687cb67e55af955dc8874a072e087ce3ed": "b51e31b93bc1bfcb23a9226ab3e3c5eb",
".git/objects/a5/3b85a3abbb032cb42cc36fca256f3a0e50d93b": "ff0e5373926f77ad6ee6316c1c06cfd2",
".git/objects/a6/19843f55e60c2fc168fe9f0afc867c44799a04": "62cce156a87ced41bad31aa355bf32df",
".git/objects/a6/738207fd15542cba015ae6ddcb8789efd81397": "4f82d36facd3c57c4f4412eca2e707e8",
".git/objects/a6/d6b8609df07bf62e5100a53a01510388bd2b22": "821831c6c42de47994c16f6a1db3ea7e",
".git/objects/a8/3a187316838d5e34a2f6f7cdd043db1607fcbb": "31e4789c69eeacefffa604ffa079cd03",
".git/objects/a8/e64fdfeb26c1371bc1169043e31175cea36ef3": "ee36831733b04277c6a77d9ebfa7d16c",
".git/objects/a9/df9dddd12b9807ba7a14402fb25a9e85cc9df0": "50a070f2d2e7c1d165c9a4aeed934d91",
".git/objects/ad/2a1bd46b79acd555111c8add35b11f16320b4f": "e3498218d01e444d797366292f2fba64",
".git/objects/ad/e4aef3d67efd80b6fc40fb508782f920137112": "ede6a3e2f4dd3875a9541079d8d66cf1",
".git/objects/ae/68315fd15a85adf453af540f30165818e6c4ea": "c5f625c03ea25008d58aa161762a8bd7",
".git/objects/ae/dedc1acf0de76c34ba371069b2c6f3afda1f24": "1350bb91333e634aaf539e874b32fc5d",
".git/objects/af/13de3fea68fac8df6ffeab1e66c032d074ae47": "1b566147c456386433c70bf112bdf3b3",
".git/objects/b1/7d6368235b96413624d424dc8860201221613d": "63a451dd8e0fbb84678bd481f60f4b0f",
".git/objects/b1/c84a3740f0573f44523498ac2c557cd62325c2": "e37e2ec8e37c435863612bf496770b05",
".git/objects/b2/6b4aa576d28c8347ea58cc60623de6d0366eda": "fb6e12d490726e37fdb6db09582b14f5",
".git/objects/b2/81137608d1c84fbc7ae3d8210103a8f46c9401": "2ac4d9e19c10bd5adaf191a14fd5a67e",
".git/objects/b2/c3c794527a17a6cceb2a47038221638189c4b0": "b7a984753bf349a1c6f4cba120ea4194",
".git/objects/b3/0157d890acc15536c65fdc796139d054f72201": "d3be24bdd8597806fc57eeb8c6abf74f",
".git/objects/b3/98b3ba7b3051d152f2dab5efafab9130c0c458": "0273478ff7697e70d1a33171d6980f5f",
".git/objects/b4/3b9095ea3aad8608fdf224e29a9c79e212176a": "04c1371eb0c64c024da4c476015d21a6",
".git/objects/b4/c826fc909a0285eb66750804fdd505cd684897": "f8ace0eafc68134fcc55a586cf48c329",
".git/objects/b4/fa4d8539a5c925b9ffbc7667f843c48dad8491": "bb5ce293ded336719255866381e944f6",
".git/objects/b7/14fcfb96f9f31620e692f07432d533e3808d3d": "95dd6556c3b973bf9c2902ab5753ccd7",
".git/objects/b7/49bfef07473333cf1dd31e9eed89862a5d52aa": "36b4020dca303986cad10924774fb5dc",
".git/objects/b7/a3e29893d10ce4ac959711d5b8031760a661f0": "ff881261d97fd9b615f7b367144118ab",
".git/objects/b7/f1703964dc4f6e9d80325c4940c46870d4d9a3": "2095159f4c3634a877325eae5e6a291b",
".git/objects/b8/793d3c0d6998ce4b65b07a974a6256f6c48134": "eb7192bdc7e31e25cb173c8242d4c267",
".git/objects/b8/fa2daa3b997a298f80f21b66a7fccc8632f381": "c5739160e2c98ddf65358ff2179ad1eb",
".git/objects/b9/2a0d854da9a8f73216c4a0ef07a0f0a44e4373": "f62d1eb7f51165e2a6d2ef1921f976f3",
".git/objects/b9/3c4c30c16703f640bc38523e56204ade09399e": "d81d171c0590dfeb78e8d6c404330bb1",
".git/objects/ba/1dfb5f41e05061cbaa5aa60ce90bce5bd32caf": "a29f73373593363242e0bc3b16f7c0b7",
".git/objects/ba/f3d77f76706dbc82162dd6bc9a1365ac5be830": "3fdbb77ecc95ce55cfcedebbe781f150",
".git/objects/bb/51049d8f65640cd32e31c9df4153de0f2df70c": "4db0c94696d06cce5bf74473a4f0e6d0",
".git/objects/bd/955e43a361ff323effed73fc3a06e654146097": "bd5518eec1713a668615e86fbcf4bd3c",
".git/objects/bd/b57226d5f2bd20f11934f4903f16459cf52379": "3289c6c0ee8813484d2b33d6c6e1df10",
".git/objects/be/600f5c5210eff6555f68c5470099b1b9bd36e8": "09620b35ff0fd10ac1308fe6643b1c73",
".git/objects/be/879e55eade3b332f51f1903442aa316f9d2f68": "ce6f5c0566403df037fc64e45ee97b7e",
".git/objects/be/88a8cb7a93c5f66164f79d848ad4288aa808f3": "822e8fc82cefc76fb8ca4ac3f2212186",
".git/objects/c0/4e20caf6370ebb9253ad831cc31de4a9c965f6": "5e216dece9f226a4ac045363fbbe142a",
".git/objects/c1/0f08dc7da60c948c794965285a3fc9a649c9f2": "6fd72351b652066c9b746391906d9bfe",
".git/objects/c1/b079c9177526a66fd6f8b901588b116121bde6": "632db90fae98b122c93e08482bdd66ff",
".git/objects/c2/cfd1ca8be5e560e088589f01db07439e2b3ef6": "f8a1f0d4707100cbd59515c0b46293bc",
".git/objects/c2/efd0b608ba84712fb3e1d01122468a50c7fba1": "5841aa09610800cbb88a643fdc9ede63",
".git/objects/c2/fc51e6921713b6e6d6848cd9b03af0cb4abfa4": "1ed4b5f10c5ff948c18e99c777c90091",
".git/objects/c4/3cc9b8ba4e4ae5c6df5ed5e3f199668b7fead0": "e70044978ca80324fbe375a7e1547ad3",
".git/objects/c4/a603d4cce78b2fbd8094bd0224d4778bc8c976": "baaa8564cd95f7d84e490bc56497c929",
".git/objects/c4/df70d39da7941ef3f6dcb7f06a192d8dcb308d": "d394f8200d968d57839b22cbe657146e",
".git/objects/c5/3eb218bf3bbfb3dda23a143f1660cdd7efa74f": "c79236ce83a826cc9de9d542ef14804f",
".git/objects/c5/d1b7ca6abd91aa3bcf73be9daad7c828be440e": "484d6cd77eb07404df8e6e7cf874cbb0",
".git/objects/c7/8546fa1412654807e74430772fbc8c74b92bb9": "e9b629cdc9cec91a4123902001abbced",
".git/objects/c8/f9ed8f5cee1c98386d13b17e89f719e83555b2": "0fc2e076b543f37f90a9e7159555320c",
".git/objects/c9/30b1bbbad49a92a6f5af5b622f0c5d9bcc20bd": "e5d26abd828019e917381053dfc19ddc",
".git/objects/cc/188b77ca040758f5963afb6108df1a0ddfb091": "f154d8ef3419e0dfca43a43e5dfc6636",
".git/objects/cc/5725b315760d100f6386e6bbf09af8fe57a9f1": "29c19352d2bee0821600856656c95791",
".git/objects/cd/22076013ce8b84475eae9bb4cd6c60b5460fbe": "81c620e2d6cbe5638d6c90ee25886389",
".git/objects/cd/a946c16fb1a0e471c3ea4e71d60e5cdddd9cf4": "b45a0d3e07759276932498bec2240ecd",
".git/objects/d0/651fda5fbc1e0e728fe38d2abc65f7250c177d": "6bc02ac128f6ce47d177770d0a6103f8",
".git/objects/d0/e1f58536026aebc4f1f70e481f6993c9ff088d": "18109e32e7fc3bf04e529c14b8723746",
".git/objects/d0/ef06e7edb86cdfe0d15b4b0d98334a86163658": "4d865184682ad08c3405e3934cd91fa9",
".git/objects/d1/5b9a2304ea544c21ccea1b3ac25c6fab513b0a": "fe22405a7254b7051daa90958d056d75",
".git/objects/d3/896c98444fbe7288d434169a28c532258a4466": "65bd2275cf87203cd8cbaa19e4cce2fc",
".git/objects/d4/92d0d98c8fdc9f93ad2543bb4f531803e9df72": "e3677ae8914304b8340d8db27e7db71a",
".git/objects/d4/a748c7b8b476433bae53c917e86daff4409335": "ab6cb0babf4910723f99ed9337fd1497",
".git/objects/d5/3ef6437726b9d1558eda97582804175c0010a2": "764cc945f02614c5bb082a403a0c55e0",
".git/objects/d5/bd01648a96d50136b2a97c8bb9aa5b711f6c39": "b52bbb3aa77140bdf2d47814158cba7d",
".git/objects/d6/4d8d37255a49886049dfb3f03fc87eddefc505": "eed41b62683f3f29cc94c625960a5975",
".git/objects/d6/824fb6d8cd11dbde452538666d418161da0294": "9a3ed4ae73e4666f234e83ce7b0c8952",
".git/objects/d6/9c56691fbdb0b7efa65097c7cc1edac12a6d3e": "868ce37a3a78b0606713733248a2f579",
".git/objects/d6/9dbd8281c0c515eae1a4c2dea32b531a178178": "e4d19b0e7d0e95adb0d2923e26b478de",
".git/objects/d8/ff65b43cb377be13bdb2343142d47f7b5f31aa": "16b9a14bbaa7caac4b289a55c4fef7eb",
".git/objects/d9/c19cc17a1d6452956495c8f392c3673f08a9eb": "440b2be48264280bd461a44ecf8a8294",
".git/objects/db/aec7c528884ed3f088e1cbacd99765e01a3655": "e486564d48f399b2a7f7a378752ce4d5",
".git/objects/dc/0125acb6346c189f68903a68a8368711d504af": "25a7d32e095e495e68680c45467fbbf8",
".git/objects/dc/139d85a93101cc0f6e9db03a3e1a9f68e8dd7e": "c815b8cd55031858470b95979194ad21",
".git/objects/dc/9ada4725e9b0ddb1deab583e5b5102493aa332": "e8022082c258e4c83e27519f77484618",
".git/objects/dc/dc2306c28505ebc0b6c3a359c4d252bf626b9f": "e712bf13b0d526e57c6ee1c8acc412d4",
".git/objects/dd/db8a30c851e7ef5b16a9108934bd1217b6b43f": "e943b985c0fbc94ad354f2ada89b854d",
".git/objects/de/28db843d7df6ed23b8a7526f6b6b4a83425fe7": "797e4f7b3d8dced098c51679ff33e848",
".git/objects/df/f4f49561c816f70eaea557d889d31cf63447ac": "fccf1f0ce075ef8d3a1ae932cbc31552",
".git/objects/df/f6597e4513dcf90c13b17495c78dd1069cdb20": "55ef000b947b86ba123d2c97a13715fd",
".git/objects/e0/2012ca0a73da86edd7614aebdc24f350669305": "4cb516f7f367eb8387729d8cfc7cf9b2",
".git/objects/e0/f0a47bc08f30b550b47b01de4c9206b6824dd9": "5ef61d56b6220833037adc3dc3871ecb",
".git/objects/e1/02093243f8cf707ef1a6ddbbbc4b7a41b63b20": "270bd13db1e888aadfcd699c61f13e5f",
".git/objects/e3/3142e71bfc751ce1d7a7d46406235425786005": "71cc9d64983f89c56658f9ade44a4809",
".git/objects/e5/2ab514dfc056ffd7b281b95dcaa45235403b3a": "4fac3aeb0e27d6427dc04505b8d29f2c",
".git/objects/e5/7562c1a77458c93a30832b39c3955941616fe4": "8ad6bb9f2e237b230699c4516c8ab035",
".git/objects/e7/1a16d23d05881b554326e645083799ab9bfc5e": "35049fca5b8153e0c0cd520b5503c269",
".git/objects/e7/c5c54370372a4cdde7288a32733998d87bd767": "37c17914ddc243c1c81881069106b916",
".git/objects/e8/695924380816f58389206db77c8e9aff7ee809": "e58d85bfac35e798a77d2797cab0e898",
".git/objects/e9/2835b11f8e60dc109602c81ee61de222176446": "acf7c92b0257795b9a801a3b87deaf87",
".git/objects/eb/8ad5e3f098825347b3311cadd38f10a27f63ae": "0ba3e2cb907e697c86aee2e8c717503e",
".git/objects/eb/97953b5b4be8f96c0b6efb00a7ce80d221a7b9": "068b3b867ee4348a2738fac502e2e570",
".git/objects/eb/9b4d76e525556d5d89141648c724331630325d": "37c0954235cbe27c4d93e74fe9a578ef",
".git/objects/eb/d58e186a7edbf269631bd9b16dcf9039020c3a": "9e0bdc8ab8ec0ef874b481c613b880e6",
".git/objects/ef/5e1b9f5fec50b62e535167212bbd48b2620af0": "327c386e23f8187578196c0562670740",
".git/objects/ef/64ff0960adcee93272b8041d0db1fb6a45ce47": "16aa3147e6688e6d3fe04b00c35dcb1c",
".git/objects/ef/a2cddc2482cfb47551708cffb83d0f35a3d958": "48b2639d164358ab9f389e6632513505",
".git/objects/ef/c2057c1e625389b934bc85d9d5b250a87b9c80": "676c9a7fd7a6a71e2d597c729d764dd2",
".git/objects/f0/83318e09ca1b6b8484b1694a149f0c5d5cac1d": "541f4960a70ea9c895b2c21196ef40a7",
".git/objects/f0/91b6b0bca859a3f474b03065bef75ba58a9e4c": "1aef8d0dea7cfdb7ffb51559c81d2fdf",
".git/objects/f2/1a1fdb9a61cfcb85c42d463071c4ee48f55761": "715353b477ec67a49612a174c9ca5231",
".git/objects/f2/b986b1849ad87d371fbebf2b6d2444b1b45df6": "b0703acb422624f63598c621b1602a43",
".git/objects/f2/e259c7c9390ff69a6bbe1e0907e6dc366848e7": "094726a4a1cc307c0a019e65a323f850",
".git/objects/f3/c28516fb38e64d88cfcf5fb1791175df078f2f": "bccf9d20d0adcc345fe8e5f4a745c4ae",
".git/objects/f4/77fae61d53a3b4cf93b9d00a244d18ed4a7826": "7672e9d61775a325c9199e76f96a4b2b",
".git/objects/f4/ab1cea3d25c3caa300b96a1d6b16a42c85739b": "14b72ab53e510129a7bab140fc39ecfb",
".git/objects/f5/bf9fa0f536c285b8c0c2ef39b9714c66fae243": "828ed007bfe4ad61a0b9a49206f1148f",
".git/objects/f7/10651d78c85ac23770de0e1f146e71006c74a1": "b40e4999c2ace2567fad4e7f612be9a4",
".git/objects/f7/4085f3f6a2b995f8ad1f9ff7b2c46dc118a9e0": "c04177ec14f012cb2c99851a5e1ca941",
".git/objects/f8/257d3482426e19dbb748c1b8c69a3787b3a76d": "c72def5bfecc78b0358efe3ef1f18033",
".git/objects/f9/0a7b9da0e8302569addb667d8e69d46e6cf02d": "4782771c0c7d125c0283d8db47295042",
".git/objects/f9/b0d7c5ea15f194be85eb6ee8e6721a87ff4644": "db1ee38473c66e5ffa9d28f6dfe66938",
".git/objects/fa/5adde060877ef60129770801c82f67df9bf28d": "1c1063964131774ad54c88d252156100",
".git/objects/fa/9c8df907dfba3f04416157b5c8486f3c7c5995": "a3258ca9b4addf05688cfdb0b0561aa2",
".git/objects/fa/c9686b2260a353277cfa1fd0a39b504a939461": "6e155713ea4154ed141c41cc9e68299a",
".git/objects/fc/565ec170b7fd55cbf422b8188a6f64eddda60e": "c1d5b622c8e63d1865785872c9d4a21d",
".git/refs/heads/master": "c50e986f06af69f1a4bd1e0eba3cbd4f",
"assets/AssetManifest.json": "34348e962f91f0c42c4fbd3ce18b83ca",
"assets/assets/note1.wav": "a2f02d4e6e9d225ebf4e554c0fc9b36b",
"assets/assets/note2.wav": "1760f9313cae8fa1564e9091c58cdcf4",
"assets/assets/note3.wav": "ec8173470a0186fdecf2f6935260eb56",
"assets/assets/note4.wav": "7817b629210c6d879b493439bcded62b",
"assets/assets/note5.wav": "69f4c1dd58b17a67a632ed106f203afc",
"assets/assets/note6.wav": "c713df4bb4bb1b6b11d041d97387d959",
"assets/assets/note7.wav": "bf1ffa7743bbdfdca078cb3f6d80bb77",
"assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"assets/fonts/MaterialIcons-Regular.otf": "95db9098c58fd6db106f1116bae85a0b",
"assets/NOTICES": "dd6a4e4cc9cf98be2d489be429c4015e",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "115e937bb829a890521f72d2e664b632",
"assets/shaders/ink_sparkle.frag": "ae6c1fd6f6ee6ee952cde379095a8f3f",
"canvaskit/canvaskit.js": "2bc454a691c631b07a9307ac4ca47797",
"canvaskit/canvaskit.wasm": "bf50631470eb967688cca13ee181af62",
"canvaskit/profiling/canvaskit.js": "38164e5a72bdad0faa4ce740c9b8e564",
"canvaskit/profiling/canvaskit.wasm": "95a45378b69e77af5ed2bc72b2209b94",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"flutter.js": "f85e6fb278b0fd20c349186fb46ae36d",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"index.html": "99ca7171c10615172190ecfd98b43e08",
"/": "99ca7171c10615172190ecfd98b43e08",
"main.dart.js": "c4a612e5d7009a07371752c8a5d51703",
"manifest.json": "a7002a22ca69766f0b11007d1629d7f1",
"version.json": "f26c93616b3146ad7ba0f6981f48402b"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "main.dart.js",
"index.html",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache only if the resource was successfully fetched.
        return response || fetch(event.request).then((response) => {
          if (response && Boolean(response.ok)) {
            cache.put(event.request, response.clone());
          }
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
